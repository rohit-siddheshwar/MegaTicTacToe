package com.amazingapps.rgs.mega_tic_tac_toe;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class SelectPlayerSignActivity extends ActionBarActivity {

    int f;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_player_sign);
        f = getIntent().getIntExtra("type",0);
    }

    public void onXClick(View view) {
        GamePlay.player1Sign = "X";
        GamePlay.player2Sign = "0";
        if(f == 1) {
            Intent i = new Intent(this, OnePlayerGame.class);
            startActivity(i);
        }
        else
        {
            Intent i = new Intent(this,TwoPlayerGame.class);
            startActivity(i);
        }
    }

    public void on0Click(View view) {
        GamePlay.player1Sign = "0";
        GamePlay.player2Sign = "X";
        if(f == 1) {
            Intent i = new Intent(this, OnePlayerGame.class);
            startActivity(i);
        }
        else
        {
            Intent i = new Intent(this,TwoPlayerGame.class);
            startActivity(i);
        }
    }
}
