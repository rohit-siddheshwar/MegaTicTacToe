package com.amazingapps.rgs.mega_tic_tac_toe;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;


public class WinnerWho extends ActionBarActivity {

    TextView result;
    PublisherInterstitialAd mPublisherInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner_who);
        result = (TextView) findViewById(R.id.result);
        if(getIntent().getIntExtra("Won",0) == 1)
        {
            result.setText("Player 1 Won The Game.");
        }
        else if(getIntent().getIntExtra("Won",0) == 2)
        {
            result.setText("Player 2 Won The Game");
        }
        else if(getIntent().getIntExtra("Won",0) == 3)
        {
            result.setText("It was a DRAW. Brilliant game !!");
        }
        else if(getIntent().getIntExtra("Won",0) == 4)
        {
            result.setText("You lost the game.");
        }
        else if(getIntent().getIntExtra("Won",0) == 5)
        {
            result.setText("You Won the game.");
        }
        mPublisherInterstitialAd = new PublisherInterstitialAd(this);
        mPublisherInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id));
        requestNewInterstitial();

        mPublisherInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
                Intent i = new Intent(WinnerWho.this , Splash.class);
                startActivity(i);
            }
        });
    }

    public void playAgain(View view) {
        if (mPublisherInterstitialAd.isLoaded()) {
            mPublisherInterstitialAd.show();
        } else {
            Intent i = new Intent(this, Splash.class);
            startActivity(i);
        }
    }

    private void requestNewInterstitial() {
        PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();

        mPublisherInterstitialAd.loadAd(adRequest);
    }
}
