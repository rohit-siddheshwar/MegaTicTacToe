package com.amazingapps.rgs.mega_tic_tac_toe;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import java.util.ArrayList;

/**
 * Created by root on 16/6/15.
 */
public class BoardAdapter extends BaseAdapter {

    ArrayList<String> boardDisplay;
    Context mContext;
    LayoutInflater inflater;

    public BoardAdapter(ArrayList<String> boardDisplay, Context mContext) {
        this.boardDisplay = boardDisplay;
        this.mContext = mContext;

        inflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return boardDisplay.size();
    }

    @Override
    public Object getItem(int position) {
        return boardDisplay.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("NewApi")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.cell,null);
        }
        Button button = (Button) convertView.findViewById(R.id.grid_item);
        float dpi = ScreenDensity(mContext);
        float width = ScreenWidth(mContext);
        float height = ScreenHeight(mContext);
        button.setWidth((int)width/10);
        button.setHeight((int)width / 10);
        button.setMinHeight((int)width / 10);
        Log.d("size",""+dpi);
        Log.d("size",""+(int)((height/width) * 12));
        //button.setTextSize(11 + dpi/161);
        button.setTextSize(11 + (int)(dpi/161));
        button.setText(boardDisplay.get(position));
        int gr,gc;
        gr = position/27;
        gc = (position/3)%3;
        if((gr + gc)%2 == 1)
        {
            button.setBackgroundResource(R.drawable.background_of_board_button_odd);
        }
        else if(gr == 1 && gc == 1)
        {
            GamePlay.groupR = gr;
            GamePlay.groupC = gc;
            button.setBackgroundResource(R.drawable.background_of_board_button_highlight);
        }
        else
        {
            button.setBackgroundResource(R.drawable.background_of_board_button);
        }
        return convertView;
    }

    public static int ScreenWidth(Context ctx) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) ctx).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;
    }

    public static int ScreenHeight(Context ctx) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) ctx).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.heightPixels;
    }
    public static int ScreenDensity(Context ctx) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) ctx).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.densityDpi;
    }
}
