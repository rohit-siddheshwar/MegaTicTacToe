package com.amazingapps.rgs.mega_tic_tac_toe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 25/6/15.
 */
public class Cpu {
    static int c = 0;
    static int flag = 0;
    static ArrayList<Integer> placable= new ArrayList<>();

    public static int playGame(){
        c=0;
        for(int g=0;g<3;g++)
        {
            for(int t=0;t<3;t++)
            {
                if(GamePlay.logicBoardCpu[g][t] != 0)
                {
                    c++;
                }
            }
        }
        if(GamePlay.miniBoard[GamePlay.groupR][GamePlay.groupC] == null)
        {
            checkPlayableGroupWinnable();
            if(c >= 7 && placable.size() < 1 )
            {
                checkPlayableByLogic();
            }
            if(placable.size() < 1)
            {
                checkAllPossiblePlayable();
            }
        }
        else
        {
            for(int k=0;k<3;k++)
            {
                for(int l=0;l<3;l++)
                {
                    if(GamePlay.miniBoard[k][l] == null)
                        checkPlacablePositions(k, l);
                }
            }
            if(placable.size() < 1) {
                for (int k = 0; k < 3; k++) {
                    for (int l = 0; l < 3; l++) {
                        if (GamePlay.miniBoard[k][l] == null)
                            checkPlacableOtherPositions(k, l);
                    }
                }
            }
            if(placable.size() < 1)
            {
                for(int aa = 0;aa<3;aa++)
                {
                    for(int bb=0;bb<3;bb++)
                    {
                        if(GamePlay.miniBoard[aa][bb] == null)
                        {
                            for (int cc = 0; cc < 3; cc++) {
                                for (int dd = 0; dd < 3; dd++) {
                                    if (GamePlay.board[aa][bb][cc][dd] == null) {
                                        placable.add((aa * 3 + cc) * 9 + (bb * 3 + dd));
                                        System.out.println("1 : "+aa+" "+bb+" "+((aa * 3 + cc) * 9 + (bb * 3 + dd)));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if(placable.size() != 0) {
            int p = placable.get((int) ((Math.random()) * 983465858) % placable.size());
            placable.clear();
            return p;
        }
        return -1;
    }

    private static void checkAllPossiblePlayable() {
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                if(GamePlay.board[GamePlay.groupR][GamePlay.groupC][i][j] == null)
                {
                    if(!placable.contains((GamePlay.groupR * 3 + i) * 9 + (GamePlay.groupC * 3 + j))){
                        placable.add((GamePlay.groupR * 3 + i) * 9 + (GamePlay.groupC * 3 + j));
                        System.out.println("2 : "+GamePlay.groupR+" "+GamePlay.groupC+" "+((GamePlay.groupR * 3 + i) * 9 + (GamePlay.groupC * 3 + j)));
                    }

                }
            }
        }
    }

    private static void checkPlayableByLogic() {
        int mini = 0,minj = 0,max=0;
        int minib = 0,minjb = 0;
        int flag = 0;
        int myCount = 0;
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                if(GamePlay.miniBoard[i][j] == null) {
                    if (GamePlay.board[GamePlay.groupR][GamePlay.groupC][i][j] == null) {
                        if (GamePlay.logicBoardCpu[i][j] >= max && GamePlay.logicBoardCpu[i][j] != -1) {
                            myCount++;
                            flag = 1;
                            minib = mini;
                            mini = i;
                            minjb = minj;
                            minj = j;
                            max = GamePlay.logicBoardCpu[mini][minj];
                        }
                    }
                }
            }
        }
        if(flag == 1) {
            placable.add((GamePlay.groupR * 3 + mini) * 9 + (GamePlay.groupC * 3 + minj));
            System.out.println("3 : "+GamePlay.groupR+" "+GamePlay.groupC+" "+((GamePlay.groupR * 3 + mini) * 9 + (GamePlay.groupC * 3 + minj)));
            if(myCount >= 2 && GamePlay.board[GamePlay.groupR][GamePlay.groupC][minib][minjb] == null) {
                placable.add((GamePlay.groupR * 3 + minib) * 9 + (GamePlay.groupC * 3 + minjb));
                System.out.println("4 : " + GamePlay.groupR + " " + GamePlay.groupC + " " + ((GamePlay.groupR * 3 + minib) * 9 + (GamePlay.groupC * 3 + minjb)));
            }
        }

        mini = minj = minib = minjb = max = flag = 0;
        myCount = 0;
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                if(GamePlay.board[GamePlay.groupR][GamePlay.groupC][i][j] == null && GamePlay.miniBoard[i][j] == null)
                {
                    if(GamePlay.logicBoardUser[i][j] <= max && GamePlay.logicBoardUser[i][j] != -1)
                    {
                        myCount++;
                        flag = 1;
                        minib = mini;
                        mini = i;
                        minjb = minj;
                        minj = j;
                        max = GamePlay.logicBoardCpu[mini][minj];
                    }
                }
            }
        }

        if(flag == 1) {
            placable.add((GamePlay.groupR * 3 + mini) * 9 + (GamePlay.groupC * 3 + minj));
            System.out.println("5 : "+GamePlay.groupR+" "+GamePlay.groupC+" "+((GamePlay.groupR * 3 + mini) * 9 + (GamePlay.groupC * 3 + minj)));
            if(myCount >= 2 && GamePlay.board[GamePlay.groupR][GamePlay.groupC][minib][minjb] == null) {
                placable.add((GamePlay.groupR * 3 + minib) * 9 + (GamePlay.groupC * 3 + minjb));
                System.out.println("4 : " + GamePlay.groupR + " " + GamePlay.groupC + " " + ((GamePlay.groupR * 3 + minib) * 9 + (GamePlay.groupC * 3 + minjb)));
            }
        }
    }

    private static void checkPlayableGroupWinnable() {
        int i = GamePlay.groupR;
        int j = GamePlay.groupC;

        checkPlacablePositions(i, j);
        if(placable.size()<1)
        {
            checkPlacableOtherPositions(i,j);
        }
    }

    private static void checkPlacablePositions(int i, int j) {
        int positionPossible = -1;
        for (int a = 0; a < 3; a++) {
            flag = 0;
            for (int b = 0; b < 3; b++) {
                if (GamePlay.board[i][j][a][b] != null ) {
                    if(GamePlay.board[i][j][a][b].equals(GamePlay.player2Sign))
                        flag += 1;
                    else {
                        flag = 0;
                        break;
                    }
                }
                else
                {
                    positionPossible = (i * 3 + a) * 9 + (j * 3 + b);
                }
            }
            if(flag == 2) {
                if(positionPossible > -1) {
                    placable.add(positionPossible);
                }
                System.out.println("7 : "+i+" "+j+" "+positionPossible);
            }
        }
//---------------------------------------------------------------------------------------------//
        for (int a = 0; a < 3; a++) {
            flag = 0;
            for (int b = 0; b < 3; b++) {
                if (GamePlay.board[i][j][b][a] != null ) {
                    if(GamePlay.board[i][j][b][a].equals(GamePlay.player2Sign))
                        flag += 1;
                    else {
                        flag = 0;
                        break;
                    }
                }
                else
                {
                    positionPossible = (i * 3 + b) * 9 + (j * 3 + a);
                }
            }
            if(flag == 2) {
                if(positionPossible > -1) {
                    placable.add(positionPossible);
                    System.out.println("8 : "+i+" "+j+" "+positionPossible);
                }

            }
        }
//---------------------------------------------------------------------------------------------//
        flag = 0;
        for (int b = 0; b < 3; b++) {
            if (GamePlay.board[i][j][b][b] != null){
                if(GamePlay.board[i][j][b][b].equals(GamePlay.player2Sign))
                    flag += 1;
                else {
                    flag = 0;
                    break;
                }
            }
            else
            {
                positionPossible = (i * 3 + b) * 9 + (j * 3 + b);
            }
        }
        if(flag == 2) {
            if(positionPossible > -1) {
                placable.add(positionPossible);
            }
            System.out.println("9 : "+i+" "+j+" "+positionPossible);
        }
//---------------------------------------------------------------------------------------------//
        flag = 0;
        for (int b = 0; b < 3; b++) {
            if (GamePlay.board[i][j][b][2-b] != null ){
                if(GamePlay.board[i][j][b][2-b].equals(GamePlay.player2Sign))
                    flag += 1;
                else {
                    flag = 0;
                    break;
                }
            }
            else
            {
                positionPossible = (i * 3 + b) * 9 + (j * 3 + 2-b);
            }
        }
        if(flag == 2) {
            if(positionPossible > -1) {
                placable.add(positionPossible);
            }
            System.out.println("10 : "+i+" "+j+" "+positionPossible);
        }
//---------------------------------------------------------------------------------------------//
    }


    private static void checkPlacableOtherPositions(int i, int j) {
        int positionPossible = -1;
        for (int a = 0; a < 3; a++) {
            flag = 0;
            for (int b = 0; b < 3; b++) {
                if (GamePlay.board[i][j][a][b] != null ) {
                    if(GamePlay.board[i][j][a][b].equals(GamePlay.player1Sign))
                        flag += 1;
                    else {
                        flag = 0;
                        break;
                    }
                }
                else
                {
                    positionPossible = (i * 3 + a) * 9 + (j * 3 + b);
                }
            }
            if(flag == 2) {
                if(positionPossible > -1) {
                    placable.add(positionPossible);
                }
                System.out.println("11 : "+i+" "+j+" "+positionPossible);
            }
        }
//---------------------------------------------------------------------------------------------//
        for (int a = 0; a < 3; a++) {
            flag = 0;
            for (int b = 0; b < 3; b++) {
                if (GamePlay.board[i][j][b][a] != null ) {
                    if(GamePlay.board[i][j][b][a].equals(GamePlay.player1Sign))
                        flag += 1;
                    else {
                        flag = 0;
                        break;
                    }
                }
                else
                {
                    positionPossible = (i * 3 + b) * 9 + (j * 3 + a);
                }
            }
            if(flag == 2) {
                if(positionPossible > -1) {
                    placable.add(positionPossible);
                }
                System.out.println("12 : "+i+" "+j+" "+positionPossible);
            }
        }
//---------------------------------------------------------------------------------------------//
        flag = 0;
        for (int b = 0; b < 3; b++) {
            if (GamePlay.board[i][j][b][b] != null){
                if(GamePlay.board[i][j][b][b].equals(GamePlay.player1Sign))
                    flag += 1;
                else {
                    flag = 0;
                    break;
                }
            }
            else
            {
                positionPossible = (i * 3 + b) * 9 + (j * 3 + b);
            }
        }
        if(flag == 2) {
            if(positionPossible > -1) {
                placable.add(positionPossible);
            }
            System.out.println("13 : "+i+" "+j+" "+positionPossible);
        }
//---------------------------------------------------------------------------------------------//
        flag = 0;
        for (int b = 0; b < 3; b++) {
            if (GamePlay.board[i][j][b][2-b] != null ){
                if(GamePlay.board[i][j][b][2-b].equals(GamePlay.player1Sign))
                    flag += 1;
                else {
                    flag = 0;
                    break;
                }
            }
            else
            {
                positionPossible = (i * 3 + b) * 9 + (j * 3 + 2-b);
            }
        }
        if(flag == 2) {
            if(positionPossible > -1) {
                placable.add(positionPossible);
            }
            System.out.println("14 : "+i+" "+j+" "+positionPossible);
        }
//---------------------------------------------------------------------------------------------//
    }


}
