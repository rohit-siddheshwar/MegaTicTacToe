package com.amazingapps.rgs.mega_tic_tac_toe;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.transition.Fade;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;


public class Splash extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ImageView v = (ImageView) findViewById(R.id.logo_image);
        Animation animation = AnimationUtils.loadAnimation(this,R.anim.logo_animator);
        v.setAnimation(animation);
    }

    public void selectPlayer(View view) {
        Intent i = new Intent(this,PlayTypeSelector.class);
        startActivity(i);
    }

    public void viewRules(View view) {
        Intent i = new Intent(this,RulesActivity.class);
        startActivity(i);
    }
}
