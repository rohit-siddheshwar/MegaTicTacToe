package com.amazingapps.rgs.mega_tic_tac_toe;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;

public class TwoPlayerGame extends ActionBarActivity {

    GridView playBoard;
    LinearLayout ll;
    ArrayList<String> values;
    Button smallInfograph;
    TextView bigInfo;
    Button button;
    Drawable drawable;
    Animation zoom;
    Animation appear;
    Animation rotate;
    Handler handler;
    android.os.Handler handler2;
    static int clickCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_player_game);

        playBoard = (GridView) findViewById(R.id.board);
        ll = (LinearLayout) findViewById(R.id.containerBoard);

        smallInfograph = (Button) findViewById(R.id.small_info);
        bigInfo = (TextView) findViewById(R.id.who_plays);

        values = new ArrayList<>();
        zoom = AnimationUtils.loadAnimation(this, R.anim.button_animate);
        appear = AnimationUtils.loadAnimation(this,R.anim.taken_highlight_animation);
        rotate = AnimationUtils.loadAnimation(this,R.anim.highlight_animation);
        handler = new android.os.Handler();
        handler2 = new android.os.Handler();
        reset();

        final BoardAdapter boardAdapter = new BoardAdapter(values, TwoPlayerGame.this);
        playBoard.setAdapter(boardAdapter);

        playBoard.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if ((GamePlay.miniBoard[position / 27][(position / 3) % 3] == null)) {
                    if ((position / 27 == GamePlay.groupR) && ((position / 3) % 3 == GamePlay.groupC)) {
                        button = (Button) playBoard.getChildAt(position);
                        drawable = button.getBackground();
                        final int pos;
                        pos = position;
                        button.setBackgroundResource(R.drawable.background_on_play);
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                button.setBackgroundDrawable(drawable);
                                checkIfBoardEmpty(pos);
                            }
                        },500);

                    } else if (GamePlay.miniBoard[GamePlay.groupR][GamePlay.groupC] != null) {
                        button = (Button) playBoard.getChildAt(position);
                        drawable = button.getBackground();
                        final int pos;
                        pos = position;
                        button.setBackgroundResource(R.drawable.background_on_play);
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                button.setBackgroundDrawable(drawable);
                                checkIfBoardEmpty(pos);
                            }
                        },500);
                    } else {
                        Toast.makeText(TwoPlayerGame.this, "You can only select a square from the area in red", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(TwoPlayerGame.this, "This Area Is Occupied.. You can play your chance anywhere in this Board.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void checkIfBoardEmpty(int position) {
        String s;
        if (GamePlay.board[position / 27][(position / 3) % 3][(position / 9) % 3][(position % 9) % 3] == null) {
            clickCount++;
            if (clickCount % 2 == 1) {
                smallInfograph.setText(GamePlay.player2Sign);
                bigInfo.setText("Player 2's Turn");
                ll.setBackgroundColor(getResources().getColor(R.color.primaryLight));
                GamePlay.board[position / 27][(position / 3) % 3][(position / 9) % 3][(position % 9) % 3] = GamePlay.player1Sign;
                s = GamePlay.player1Sign;
                onPlay(position, GamePlay.player1Sign);
            } else {
                smallInfograph.setText(GamePlay.player1Sign);
                bigInfo.setText("Player 1's Turn");
                ll.setBackgroundColor(getResources().getColor(R.color.primary));
                GamePlay.board[position / 27][(position / 3) % 3][(position / 9) % 3][(position % 9) % 3] = GamePlay.player2Sign;
                s = GamePlay.player2Sign;
                onPlay(position, GamePlay.player2Sign);
            }

            int result = GamePlay.playGame(position / 27, (position / 3) % 3, (position / 9) % 3, (position % 9) % 3, s, this, playBoard);
            afterPlay(position);
            if (result == 1) {
                afterWinViewChange();
                handler2.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent(TwoPlayerGame.this, WinnerWho.class);
                        i.putExtra("Won", 1);
                        startActivity(i);
                        finish();
                    }
                }, 500);
            } else if (result == 2) {
                afterWinViewChange2();
                handler2.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent(TwoPlayerGame.this, WinnerWho.class);
                        i.putExtra("Won", 4);
                        startActivity(i);
                        finish();
                    }
                }, 500);
            } else if (result == 3) {
                afterDrawViewChange();
                handler2.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent(TwoPlayerGame.this, WinnerWho.class);
                        i.putExtra("Won", 3);
                        startActivity(i);
                        finish();
                    }
                }, 500);
            }

        } else {
            Toast.makeText(TwoPlayerGame.this, "This box is already played.", Toast.LENGTH_SHORT).show();
        }
    }



    private void afterDrawViewChange() {
        for (int u = 0; u < 81; u++) {
            button = (Button) playBoard.getChildAt(u);
            button.setBackgroundResource(R.drawable.background_draw);
            button.setText("");
            button.setAnimation(zoom);
        }
    }

    private void afterWinViewChange2() {
        for (int u = 0; u < 81; u++) {
            button = (Button) playBoard.getChildAt(u);
            if (GamePlay.player2Sign.equals("X")) {
                button.setBackgroundResource(R.drawable.background_x_highlight);
            } else {
                button.setBackgroundResource(R.drawable.background_o_highlight);
            }
            button.setText("");
            button.setAnimation(zoom);
        }
    }

    private void afterWinViewChange() {
        for (int u = 0; u < 81; u++) {
            button = (Button) playBoard.getChildAt(u);
            if (GamePlay.player1Sign.equals("X")) {
                button.setBackgroundResource(R.drawable.background_x_highlight);
            } else {
                button.setBackgroundResource(R.drawable.background_o_highlight);
            }
            button.setText("");
            button.setAnimation(zoom);
        }
    }

    private void onPlay(int position, String sign) {

        int changeIndex;
        for (int o = 0; o <= 2; o++) {
            for (int in = 0; in <= 2; in++) {
                changeIndex = (GamePlay.groupR * 3 + o) * 9 + (GamePlay.groupC * 3 + in);
                if (GamePlay.nomarks[changeIndex] == 0) {
                    button = (Button) playBoard.getChildAt(changeIndex);
                    if ((GamePlay.groupR + GamePlay.groupC) % 2 == 0)
                        button.setBackgroundResource(R.drawable.background_of_board_button);
                    else
                        button.setBackgroundResource(R.drawable.background_of_board_button_odd);
                } else {
                    button = (Button) playBoard.getChildAt(changeIndex);
                    if (GamePlay.nomarks[changeIndex] == 1) {
                        button.setBackgroundResource(R.drawable.background_x_highlight);
                    }
                    else if (GamePlay.nomarks[changeIndex] == 2)
                        button.setBackgroundResource(R.drawable.background_x_highlight_light);
                    else if (GamePlay.nomarks[changeIndex] == 3) {
                        button.setBackgroundResource(R.drawable.background_o_highlight);
                    }
                    else if (GamePlay.nomarks[changeIndex] == 4)
                        button.setBackgroundResource(R.drawable.background_o_highlight_light);
                    else if (GamePlay.nomarks[changeIndex] == 5) {
                        button.setBackgroundResource(R.drawable.background_draw);
                    }
                }
            }
        }
        button = (Button) playBoard.getChildAt(position);

        button.setText(sign);


    }

    private void afterPlay(int position) {
        int r, c;
        r = position / 9;
        c = position % 9;
        GamePlay.groupR = r % 3;
        GamePlay.groupC = c % 3;

        int index;
        for (int o = 0; o <= 2; o++) {
            for (int in = 0; in <= 2; in++) {
                index = (GamePlay.groupR * 3 + o) * 9 + (GamePlay.groupC * 3 + in);
                if (GamePlay.nomarks[index] == 0) {
                    button = (Button) playBoard.getChildAt(index);
                    button.setBackgroundResource(R.drawable.background_of_board_button_highlight);
                    button.startAnimation(rotate);
                } else if(GamePlay.nomarks[index] == 1){
                    button = (Button) playBoard.getChildAt(index);
                    button.setBackgroundResource(R.drawable.background_highlight_red);
                    button.startAnimation(appear);
                } else if(GamePlay.nomarks[index] == 3){
                    button = (Button) playBoard.getChildAt(index);
                    button.setBackgroundResource(R.drawable.background_highlight_red);
                    button.startAnimation(appear);
                }
                else if(GamePlay.nomarks[index] == 5){
                    button = (Button) playBoard.getChildAt(index);
                    button.setBackgroundResource(R.drawable.background_highlight_red);
                    button.startAnimation(appear);
                }
            }
        }
    }

    private void reset() {
        clickCount = 0;
        int cc = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                GamePlay.miniBoard[i][j] = null;
                for (int k = 0; k < 3; k++) {
                    for (int l = 0; l < 3; l++) {
                        GamePlay.nomarks[cc] = 0;
                        cc++;
                        GamePlay.board[i][j][k][l] = null;
                        values.add("");
                    }
                }
            }
        }

    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Really Exit the Game?")
                .setMessage("Are you sure you want to Leave the game?\nThis will erase all saved game data.")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        TwoPlayerGame.super.onBackPressed();
                    }
                }).create().show();
    }

    public void newGame(View view) {
        new AlertDialog.Builder(this)
                .setTitle("New Game?")
                .setMessage("Are you sure you want to Leave the existing game?\nThis will erase all saved game data.")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        TwoPlayerGame.super.onBackPressed();
                    }
                }).create().show();
    }
}
