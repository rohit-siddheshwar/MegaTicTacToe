package com.amazingapps.rgs.mega_tic_tac_toe;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.GridView;

/**
 * Created by root on 9/6/15.
 */
public class GamePlay {
    static int[][] logicBoardUser = new int[3][3];
    static int[][] logicBoardCpu = new int[3][3];
    static int[] nomarks = new int[81];
    static String[][][][] board = new String[3][3][3][3];
    static String[][] miniBoard = new String[3][3];
    static String player1Sign;
    static String player2Sign;
    static Animation zoom;
    static int groupR, groupC;
    static int flag = 0;
    static int countP1=0;
    static int countP2=0;

    public static int playGame(int i, int j, int k, int l, String s,Context mContext, GridView playBoard) {
        //showLog();
        zoom = AnimationUtils.loadAnimation(mContext,R.anim.button_animate);
        if(s.equals(player1Sign))
        {
            logicBoardUser[i][j] += 1;
        }
        else if(s.equals(player2Sign))
        {
            logicBoardCpu[i][j] += 1;
        }
        board[i][j][k][l] = s;
        for (int a = 0; a < 3; a++) {
            flag = 0;
            for (int b = 0; b < 3; b++) {
                if(board[i][j][a][b] != null) {
                    if (!board[i][j][a][b].equals(s)) {
                        flag = 1;
                    }
                }
                else
                    flag = 1;
            }
            if (flag == 0) {
                changeView(i, j, s, mContext, playBoard);
                return updateMiniBoard(i, j, s);
            }
        }
//---------------------------------------------------------------------------------------------//
        for (int a = 0; a < 3; a++) {
            flag = 0;
            for (int b = 0; b < 3; b++) {
                if(board[i][j][b][a] != null) {
                    if (!board[i][j][b][a].equals(s)) {
                        flag = 1;
                    }
                }
                else
                    flag = 1;
            }
            if (flag != 1) {
                changeView(i, j, s, mContext, playBoard);
                return updateMiniBoard(i, j, s);
            }
        }
//---------------------------------------------------------------------------------------------//
        flag = 0;
        for (int b = 0; b < 3; b++) {
            if(board[i][j][b][b] != null) {
                if (!board[i][j][b][b].equals(s)) {
                    flag = 1;
                }
            }
            else
                flag = 1;
        }
        if (flag != 1) {
            changeView(i, j, s, mContext, playBoard);
            return updateMiniBoard(i, j, s);
        }
//---------------------------------------------------------------------------------------------//
        flag = 0;
        for (int b = 0; b < 3; b++) {
            if(board[i][j][b][2-b] != null) {
                if (!board[i][j][b][2-b].equals(s)) {
                    flag = 1;
                }
            }
            else
                flag = 1;
        }
        if (flag != 1) {
            changeView(i, j, s, mContext, playBoard);
            return updateMiniBoard(i, j, s);
        }
//---------------------------------------------------------------------------------------------//

        flag = 0;
        for (int a = 0; a < 3; a++) {
            for (int b = 0; b < 3; b++) {
                if(board[i][j][b][a] == null) {
                        flag = 1;
                }
            }
        }
        if (flag == 0) {
            changeView(i, j, "r", mContext, playBoard);
            logicBoardCpu[i][j] = -1;
            logicBoardUser[i][j] = -1;
            return updateMiniBoard(i, j, "r");
        }
        return 0;
    }

    private static void showLog() {
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");
        for(int m=0;m<3;m++)
        {
            for(int n=0;n<3;n++)
            {
                for(int p=0;p<3;p++)
                {
                    for(int q=0;q<3;q++)
                    {
                        if(board[m][p][n][q] == null)
                            System.out.print("- ");
                        else
                            System.out.print(board[m][p][n][q] + " ");
                    }
                    System.out.print("\t");
                }
                System.out.println();
            }
        }
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");
    }

    private static void changeView(int i, int j, String s, Context mContext, GridView playBoard) {
        int changeIndex;
        for(int o=0;o<=2;o++)
        {
            for(int in=0;in<=2;in++)
            {
                changeIndex =(i*3 + o)*9 + (j*3 + in);
                Button button = (Button) playBoard.getChildAt(changeIndex);

                switch (s) {
                    case "X":

                        if (o == in || o == 2 - in) {
                            nomarks[changeIndex] = 1;
                            button.setBackgroundResource(R.drawable.background_x_highlight);
                            button.setText("");
                            button.setAnimation(zoom);
                        } else {
                            nomarks[changeIndex] = 2;
                            button.setBackgroundResource(R.drawable.background_x_highlight_light);
                            button.setText("");
                        }
                        break;
                    case "0":

                        if (o == in && in == 1) {
                            nomarks[changeIndex] = 4;
                            button.setBackgroundResource(R.drawable.background_o_highlight_light);
                            button.setText("");
                        } else {
                            nomarks[changeIndex] = 3;
                            button.setBackgroundResource(R.drawable.background_o_highlight);
                            button.setText("");
                            button.setAnimation(zoom);
                        }
                        break;
                    case "r":
                        nomarks[changeIndex] = 5;
                        button.setBackgroundResource(R.drawable.background_draw);
                        button.setText("");
                        button.setAnimation(zoom);
                        break;
                }
            }
        }
    }

    public static int updateMiniBoard(int i, int j, String s) {
        miniBoard[i][j] = s;
        if(s.equals(player1Sign))
        {
            logicBoardUser[i][j] = -1;
        }
        else if(s.equals(player2Sign))
        {
            logicBoardCpu[i][j] = -1;
        }
        for (int a = 0; a < 3; a++) {
            flag = 0;
            for (int b = 0; b < 3; b++) {
                if(miniBoard[a][b] != null) {
                    if (!miniBoard[a][b].equals(s)) {
                        flag = 1;
                    }
                }
                else
                    flag = 1;
            }
            if (flag != 1) {
                if (s.equals(player1Sign))
                    return 1;
                else
                    return 2;
            }
        }
//---------------------------------------------------------------------------------------------//
        for (int a = 0; a < 3; a++) {
            flag = 0;
            for (int b = 0; b < 3; b++) {
                if(miniBoard[b][a] != null) {
                    if (!miniBoard[b][a].equals(s)) {
                        flag = 1;
                    }
                }
                else
                    flag = 1;
            }
            if (flag != 1) {
                if (s.equals(player1Sign))
                    return 1;
                else
                    return 2;
            }
        }
//---------------------------------------------------------------------------------------------//
        flag = 0;
        for (int b = 0; b < 3; b++) {
            if(miniBoard[b][b] != null) {
                if (!miniBoard[b][b].equals(s)) {
                    flag = 1;
                }
            }
            else
                flag = 1;
        }
        if (flag != 1) {
            if (s.equals(player1Sign))
                return 1;
            else
                return 2;
        }
//---------------------------------------------------------------------------------------------//
        flag = 0;
        for (int b = 0; b < 3; b++) {
            if(miniBoard[b][2-b] != null) {
                if (!miniBoard[b][2 - b].equals(s)) {
                    flag = 1;
                }
            }
            else
                flag = 1;
        }
        if (flag != 1) {
            if (s.equals(player1Sign))
                return 1;
            else
                return 2;
        }

        flag = 0;
        for(int a=0;a<3;a++)
        {
            for(int b=0;b<3;b++)
            {
                if(miniBoard[a][b] == null)
                {
                    flag = 1;
                }
                else
                {
                    if (miniBoard[a][b].equals(player1Sign))
                    {
                        countP1++;
                    }
                    else
                    {
                        countP2++;
                    }
                }

            }
        }
        if(flag == 0)
        {
            if(countP1 > countP2)
            {
                return 1;
            }
            else if(countP2 > countP1)
            {
                return 2;
            }
            else
                return 3;
        }
        return 0;
    }
}
